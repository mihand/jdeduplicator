clean code
==========

There are many ways to write code that correctly implements a specification.
Among these variants a small minority will be good code. Code that is easy to
read, understand and change.

The most important character of good code is can be read by someone else than the author.

What are some qualities of easy to read code?

Easy to read code will use good, readable names that inform you.
When you read a part of the code, you should not need to know everything the software is doing.
This implies that some good abstractions are present. The different concerns are separated.
There are internal api's that separate subsystems.

Good code tells a story, and you can read it top down.
Notice that most code is written bottom up, but read top down.


your client wants bugs
----------------------

In most cases clients want a change in specifications to be implemented fast, rather than correct.
One should prefer code that is easy to understand to correct code.

The bugs you left in, the hacks that break the abstractions that are needed to understand the software,
these are technical debt. You need to repay this debt, recreate abstractions that work.

After you have written code, you should take care that it can be read top-down.


Are there any rules?
--------------------

There is no set of rules that guarantee a clean codebase.
There are however some principles that, if followed, will at least limit the mess.

Nothing really substitutes for caring about readability and working towards it.


organizing data
===============

It is easier to reason about data than about code.
Business data can be modelled like database tables with has-a relationships.

The common ADT's like lists maps and queues should be the building blocks for these
data classes. Be familiar with trees and graphs.

Remember that data objects are values, so implement equality and hashcode for them.

Use immutable data types as much as possible.
For the mutable types encapsulate the state behind methods that will keep the invariants of these structures.

Think about the data transformations that the program will have to do.
This will influence a good decision on how to model data.

Prefer some duplication of data to very complex data dependencies.

Lists are easier to understand than trees, and trees easier than acyclic graphs.
Mutually recursive data is hard to understand.


organizing code
===============

algorithms
----------

By algorithm I mean code that uses if's and loops to transform and query data.
Prefer complex ADT's to complex code.
romanianCustomers.intersect(premiumCustomers) is better than nested loops.

Offload much data storage and querying to the database:
select * from customers where customer.country = ro and customer.ispremium is even better.

Try to use standard looping like map, filter, reduce, fold.

Use data structures and iterators to decouple algorithm steps and implement processing pipelines.

Prefer pure algorithms, that dont rely on mutating variables.

Linear top to bottom code is easy to read.
A for loop with an if used to avoid duplicating 4 lines of code is not worth it.


procedures
----------

Pure procedures, aka functions are easy to understand.
They don't depend on when they are called in the program.
Prefer these.

If a procedure has 5 args but in the whole program just the first 2 actually change
then the remaining 3 are not really parameters are they?
They are context. Put the procedure in a class and send those 3 to a constructor.

Do one thing.
More than 7 local mutable vars, or more than 4 nested for,if 's are too much.


classes
-------

Use them to group procedures that operate on the same data.
Use them to implement polymorphic choice.

The simplest class to understand is one that has immutable state and cannot be extended.


organizing systems
==================

You will have to define interfaces that clearly state exposed functionality.
At this layer think of plugins that interact.

Identify services, decide who instantiates and constructs them.
You should get familiar with dependency injection and service locators.

At the system level databases and similar platforms are important.
Some architectural patterns are widely used.
Learn about ORM and other frameworks.

Do not let frameworks decide for you how to organize data and code.
You may agree with them, but you should make this decision.
All you data is hibernate mapped and all services respond to http requests ?
Did you make this decision ?


Design patterns
===============

non-oop:

* iterator : for splitting algorithms into processing pipelines
* data structure based dispatch : for when polymorphism is not dynamic enough

oop

* abstract factory
  for when you don't want to know how to construct the services, just how to use one that is already set up
* decorator, adapter facade
  when you are not happy with that api
* observer / callback
  when you need to call arbitrary client code when something happened


Refactoring
===========

* rename, move
* extract method, inline
* extract interface, class


SOLID
=====

Single responsibility.

    Obviously stated, tricky in practice: don't do 2 unrelated things in the same place.

Open/Closed.

    Offer hooks to extend the functionality of your api without the client to
    have to change your source code.

Liskov.

    If you use implementation inheritance then the subclass should be
    substitutable by the superclass by your clients.
    Again obvious to state: make sure that is-a really is a is-a relationship.
    Hard in practice where we typically use subclasses to add more behaviour and variability.

Interface segregation.

    I think they needed a vowel in the acronym. This seems far less important.
    Instead of a monolithic huge api, expose smaller ones tailored for client needs.
    Ex. offer a complex but easy to hook into api like java.io streams ,
    but also a easy to use one like Files.readAll

Dependency inversion.

    Higher level code naturally depends on the lower level code behaviour.
    Break this dependency by depending on common interfaces.
    Thus allowing customization and replacement of lower level services in a
    way that is transparent to high level code.


Books
=====

On java

* Effective Java by Joshua Bloch

On clean code

* Code Complete by Steve McConnell
* Clean Code by Robert Martin
* Refactoring by Kent Beck and Martin Fowler

Expanding your horizon

* Structure and interpretation of computer programs by Gerald Sussman
* Introduction to algorithms by Rivest & Cormen
* The C programming language by Kernighan and Ritchie

Play with the folowing languages:

* a scripting language like python or ruby
* a mainstream oop language java/c#
* plain old C
* a modern lisp like clojure

