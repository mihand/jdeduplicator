Finding duplicate files
=======================

We need to find all duplicate files from a directory and all it's subdirectories.

Design
------

finding files
`````````````

First we need to obtain all the files. 
RecursiveFileWalker is responsible with this.

.. code-block:: java

    class RecursiveFileWalker {
        RecursiveFileWalker(int ignoreSmallerThan);
        List<File> getFiles(File directory) throws BadPath;
    }

It is an object because the file walking function has context data, a filter that ignores 
files smaller than a certain size. 
We need to at least ignore 0 length files as they are trivially identical.
There will be just one instance of RecursiveFileWalker so it is a service like object.

For simplicity it returns a list of files. This may take up unreasonable memory.
It this becomes a problem then we can rewrite it to return iterators.

finding duplicates
``````````````````

Now about actually finding the duplicates.
The simplest way of doing it is to check every pair of files by reading them and comparing their contents.
Reading all content in memory is a problem as large files like movies are common for this task.
This is easily solved by reading byte by byte and stopping on the first difference.
This strategy is used by DuplicateGrouper.

.. code-block:: java

    class DuplicateGrouper {
        public FileGroup<String> group(List<File> files);
    }

It returns groups of identical files.
A FileGroup is essentially just a Map<String representative, List<Files> duplicates> : 'first_duplicate_file_name': ['second', 'third']'.

Reading all pairs is impractically slow. 
Fortunalty this is wasteful and can easily be improved.
For once we compare obsiouslly different files, like those of different size.

If we group by size then in practice we will massivly reduce our list of candidates.

The data structure that fits this need of grouping is a map.
A map size --> [files with that size...]
Where we keep only those keys that have lists of more than 1 files.

This is no longer a pure map, but a derived data structure.
So we encapsulate this behaviour in the FileGroup class.

.. code-block:: java

    class FileGroup<T> {
        FileGroup<T> withoutOneElementGroups();
        static <T> FileGroup<T> groupBy(Classifier<T> classifier, List<File> files);
    }

FileGroup has the resposability of grouping the files. 
But it does not want to know that it is grouping by file size.
The criteria of grouping is abstracted by Classifier, a function like interface.

.. code-block:: java

    interface Classifier<T> {
        T classify(File file) throws IOException;
    }

It returns a hashable type that FileGroup will use to group the files.

The grouping by size worked so well! Can we do more of this?
Yes. We can compute a crypto hash of the file like md5.
It is almost imposible to get 2 files with the same hash if they were not prepared intentionally by an adversary.
So we group by md5 of the full file content.
Notice that this requires reading the files just once.

There are some in-between ideas like comparing by size and the first kb of the file content.
This costs less than reading the whole file for md5 computation.
This idea requires the notion of combining an already existing grouping, by size, with another criteria for 
grouping, the hash of the file header.

It is this method that has that responsability.

.. code-block:: java

    class FileGroup<T> {
        <U> FileGroup<Tuple<T, U>> groupBy(Classifier<U> classifier);
    }

The grouping key is the tuple: size, headear hash. It is necessarly a composed key as neither
the size nor the hash is sufficient.

All this functionality is gathered by the DeDuplicator api:

.. code-block:: java

    public class DeDuplicator{
        public List<List<File>> run(File root);
    }

It returns the list of duplicate groups, themselves a list of files.

Diagrams
````````

A typical FileGroup object instance

+-------------------+-------+
|    key            | files |
+-------+-----------+-------+
| size  |  md5 hash | files |
+=======+===========+=======+
| 2.3Mb | 234adef   | a.txt |
+-------+-----------+-------+
|                   | b.txt |
+-------+-----------+-------+
| 2.3Mb | cafee00   | u.jpg |
+-------+-----------+-------+
|                   | b.jpg |
+-------+-----------+-------+

.. uml::

    @startuml
    deduplicator -> fileWalker : getFiles
    fileWalker --> deduplicator : files[]
    deduplicator -> fileGroup: groupBy(file_size)
    fileGroup -> fileGroup: withoutOneElementGroups
    fileGroup --> deduplicator: "FileGroup size -> files"
    deduplicator -> fileGroup: groupBy(file_header)
    fileGroup -> fileGroup: withoutOneElementGroups
    fileGroup --> deduplicator: "FileGroup(size, header) -> files"
    deduplicator -> fileGroup: groupBy(file_md5)
    fileGroup -> fileGroup: withoutOneElementGroups
    fileGroup --> deduplicator: "FileGroup(size, header, md5) -> files"
    deduplicator -> duplicateGrouper : pairwiseCompare
    duplicateGrouper --> deduplicator : final_duplicates
    @enduml

