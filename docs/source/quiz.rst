Quiz
====

Discuss the following code samples.
Allow for these fake names foo bar baz frob fonfon


AA
--

.. expression level

.. code-block:: java

    for(int i=0; i < 3; i+=10)
        drawLine(i, 0, i, 10)


.. code-block:: java

    item = collection.addItem(firstItem).addItem(secondItem)


.. algorithm level

.. code-block:: java

    for (element: elements){
        int min = this.getThreshold().getMin();
        int max = this.getThreshold().getMax();
        if (element < min && element > max){
            this.badElement = true;
        }
    }


.. code-block:: java

    while(true){
        b = fileStream.read();
        if (i > 10240)
            break
        if (b == -1 || b == 435628)
            break
    }

.. code-block:: java

    for(foo: list)
        if (foo == 3)
            list.remove(foo)

.. code-block:: java

    int i = 0;
    for( foo: list){
        i++;
        ...
    }

.. code-block:: java

    while (...)
        while(...)


.. code-block:: java

    for (mapRegion: root.getRegions()){
        for (roadNode: mapRegion.getNodes("road")){
            for (roadSegmentNode: roadNode.getNodes("segment")){
                for (path: roadSegmentNode.getNodes("path")){
                    for (line: path.getNodes("geopoint")){
                        drawLine(line)


.. code-block:: java

    int findBestScoreWithinAcceptableRange(ArrayList<Integer> elements){
        for (Integer s: elements){
            if (s != null && ( s == 15 || s == 30 || s == 45 || s == 50)){
                if (s > max){
                    max = s;
                }
            }
        }
    }

.. mutable args, crazy naming and booleans

.. code-block:: java

    List<Map<String, Map<String, File>>>> foo;


.. code-block:: java

    FooBar fooBar = FooBarFactory.getFooBar()
    FooBarResult fooBarResult = fooBar.runFooBar(fooRequest)

.. code-block:: java

    void getValidOnes(List arg, List arg2, List res){
        for(int g = 0; g < arg.getList().size(); g++){
            for (int h=0; h < arg2.getList().size(); h++){
                if (arg.getList().get(g)!= null && arg.getList().get(g).equals(arg2.getList().get(h)){
                    res.add(arg.getList().get(g);
                }
        }
    }

.. code-block:: java

    /** returns the previous priority */
    int setPriority(int priority){ ... }

.. code-block:: java

    float[] floatArray = new float[((double[]) dataset).length];
    for (int i = 0; i < ((double[]) dataset).length; i++) {
            floatArray[i] = (float) ((double[]) dataset)[i];
    }

.. procedures

.. code-block:: java

    static void buildBurstTreeFromTraitTree(TraitRoot root, Options opts,
        PortletOptions portletOpts, BurstSelection preselection,
        ElideManager mgr, TraitManager traitMgr)

.. code-block:: java

    /**
     * makes an appointment for a dog at the given date
     * @param dog The dog object holding info about the patient
     * @param when The date when to reserve an appointment
     */
    void makeAppointment(Dog dog, Date when){
        if (breedsWeTreat.contains(dog.breed) && getShedule().medicsAvailable(when).size() > 0) {
            this.db.ensureConnected(this.connstr);
            this.db.insert(dog.tosql());
        }
    }

.. code-block:: java

    boolean performChecks(Client client, Policy universalPolicy, Policy customerSpecificPolicy, Policy extra){
        if (! universalPolicy.admits(client.getAccessRequirements())
            throws new UniversalPolicyDeniesRequirementsException();
        if (customerSpecificPolicy != null && ! customerSpecificPolicy.admits(client.getAccessRequirements() )
            throws new PolicyDeniesRequirementsException();
        return true;
    }

.. code-block:: java

    void doFonFon(int foo, Integer bar, boolean baz, boolean foofoo)

.. code-block:: java

    void doFonFinFon(int foo){
        acquireGPUContext
        sendGeometry
        releaseGPUContext


.. classes

.. code-block:: java

    class Point{
        private int x;
        private int y;
        int getX()
        int setX()
        ...
    }

.. code-block:: java

    class Rectangle{
        private Points[] points;
        Points[] getPoints;
        ...
    }

.. code-block:: java

    class Foober{
        void doFonFon(Bazber baz){
            baz.getSchmoo(this.requiredSchmooFlavor).getTumpaloo().setBaloo(this.baloo)
        }
    }

.. code-block:: java

    class Foober{
        void doFonFon(Bazber baz){
            ...
            schmoo = new SchmooServiceImpl(this.baloo, this.tumpaloo);
            yellowFOO = new FooServiceImple();
        }
    }

.. code-block:: java

    class Foober{
        Foo foo;
        Bar bar;
        Baz baz;
        void doBadThingsToFoo(){
            baz.doFonFon();
            // does not touch bar
        }
        void doBadThingsToBar(){
            // does not touch foo
        }


.. apis

Comment on these api's:


A json library exposes this API:

.. code-block:: java

    Json readFile(String path);

    void writeFile(Json json, String path);

Comment the api.


Another library implements common geometry operations like distance from a point to another and to a line circle etc.
The documentations states:

    While we aim to be very flexible we do not have yet support for deep mathematical computational routines.
    In this version we only compute distances.
    To compute the distance from a point to a line you first need to define a point and a space.
    The space can be a common euclidean one but we also support hyperbolic and elliptic geometries with ease.
    Just use RiemannUniverseFactory(int numberOfDimensions) instead of the EuclideanUniverseFactory.
    To define a point use the universe.newpoint factory method ...

Comment on the api.

This is part of Spring framework

.. code-block:: java

    import org.springframework.aop.framework.AbstractSingletonProxyFactoryBean
    // Convenient proxy factory bean superclass for proxy factory beans that create only singletons.


.. code-block:: java

    void foo(String s, List l, Char[] chars){
        return s.length() == l.size() && l.size() == chars.length;
    }

.. code-block:: java

    private static final

