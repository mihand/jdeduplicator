My opinion about code
=====================

This is my opinion. It differs in many aspects from the opinion of the
community and of many experts.
I encourage you to think about the topics I will discuss here.
Many statements that follow are at least debatable if not altogether false.

.. contents:: Toc
    :depth: 2

Our memories
------------

.. image :: g3851.png
    :scale: 33 %

Short term memory
~~~~~~~~~~~~~~~~~

* **7 items 18 seconds**
* no learning needed

=> at any level of abstraction 
    if the thing takes more than 18 seconds to read or 
    it involves more than 7 things, then a newbie cannot parse it.

* 7 - this many variables/functions/classes/modules
* 18 seconds - this long function class modules

Procedural memory
~~~~~~~~~~~~~~~~~

I dont know how i ride the bicycle!

* year long
* cannot be remembered willingly
* extensive learning needed by repetition

quick what does this do

.. code::

    ret = list()
    accum = 0
    for e : input
        if isfoo(e)
            accum = oper(accum, e)
            ret.add(accum)
    return ret

    i recognize it as a map, then filter-map, then filter-map-reduce

Long term memory
~~~~~~~~~~~~~~~~

* year long
* can be remembered willingly
* learning needed

How to define a 2d point class in java? with equals and hashcodee

* learn your language, the class library and the framework
* learn your product, the main systems and modules and classes
* the more you have in long term the less you fill up those 7 short term slots
* learn to type so that you don't have to look at the keyboard and sacrifice a slot
  and half a second to remembering where % is
* learn your ide's shortcuts for the same reason

The underlying tower of abstraction
-----------------------------------

.. image:: image10.png
   :scale: 30 %

.. image:: g7403.png
   :scale: 45 %

* language primitives

organized in 

* expressions

  - a + b.getFoo()
  - natural data in values

organized in 

* commands 

  - result = a + b.getFoo() 
  - data in local variables

organized in 

* algorithms

  - if for while
  - data in local data structures

organized in 

- functions

  - parameters
  - data in local data structures
    
organized in 

- classes

  - contextual data
  - data in encapsulated data structures
    
organized in 

- modules

  - class dependencies
  - data in composed object graphs

organized in 

- plugins/services

  - lifetime, initialization, activation
  - versioning, stable apis, deployable artifacts, public docs
  - data in explicit stable and standard formats

organized in 

- programs

  - orchestration - how to bind services into a flow that implements the app
  - data in databases

organized in 

- distributed systems

  - multiple async programs
  - language independent protocols
  - multiple languages/platforms
  - data in multiple databases
 

Example
~~~~~~~

A twitter clone:

- distributed systems layer

    A js app distributed to all client browsers
    talks to 
    A load balancer that picks an app server instance
    talks to
    App server and a cache server
    talks to 
    A db shard

- program layer

    The app server runs a program that has 
    discovered and loaded plugins that repond to 
    every type of request.
    Some plugin does security/login stuff.
    Another handles geolocation etc.
    A final one handles tweet publication.

- plugin layer

  The tweet publication plugin defines its api 
  and its dependencies ex the Db plugin.

- module layer
    The tweet publication plugin has a package containing tweet validation logic
    and another that contains database persitance code.

- class layer
  
  There is a tweet data structure and the allowed operation for one.
  There is a class for detecting links and bad words.

- function layer
  
  A detect links function operates on the data structure and adds metadata to it.

- algorithm layer

  A small parsing algoritm searcher for what looks like a link
  A regex engine state is kept 

Abstraction
-----------

* Abstractions should simplify not obscure
* Dont abstract speculatively, for the imagined future.
  You might never need that, and you can always change your code. 
  Unless its a public api (as in pubic for other people)
* Abstractions should create a DSL in which it is easy to write and read the code.
  A DSL where the verbs are methods and objects are subjects and ... objects
  A DSL where the names are in the problem domain.

Comments
--------

* should not make up for your abstraction dsl failure
* no useless javadocs
* public api's
* preconditions and invariants

Names
-----

* the further away it is used the longer and more descriptive
* chose them so that the code can be readable out loud:
  a4, mkFooFact, Bar bar = new BarFact().getBar()
* remember the dsl for public stuff

Functions
---------

* name boolean sub-expressions
* no reasonable function has 5 parameters.
  It has 2 or 3 and the rest is stuff that does not change at every callsite
  stuff is either context, settings or flags
  Make it a class
* use pure functions agressively
* return data structures/iterators instead of taking callbacks
* dont use return args, make a return type
  No way should you use 2 return args.
* 4 tabs or your function is broken
* dont you dare pass by global var/field to calles!

Dry
---

* dont repeat yourself a 3rd time
* code duplication especially trivial one is way better than a bad/cumbersome abstraction!

Errors
------

* be aware of exception safety
  Linear code is not linear with exceptions ::

     init() --> try { init()
     doit()           doit() }
     free()     finally{ free }
  
* be aware of exception utility
  Great for bypassing intermediate callers which do not even need to be aware of errors 
  as long as they dont aquire resources

* add resons for why it blew, what data was involved in the tragedy?
* expose an exception hierarchy at a sane granularity
  not IoError with field errno
  not NtfsBadDescriptorEntryException
* some things should be expected not Exceptions
  Match regex.findAll(re, string) 
  return empty matches dont throw
* in a language with exceptions dont use error codes
  the only niche is paranoid code with lots of resource aquisition like the c malloc/free dance
* dont return nulls, you are essentially throwing nullpointerexception

Dont overdo OOP
---------------

* you do **NOT** use a design pattern. You write your code then realize that that 
  pattern would help organize sum stuff. Then you change your code to look 
  more like the standard pattern.
* static functions in a static class are *Fine* if
  
  - all are at the same level of abstraction
  - parameters of multiple classes
  - they dont take context parameters

* A mutable circle is not an ellipse nor the other way around. 
  Dont waste time in creating hierarchies.
  Most inheritance is ontologically false. 
  Inherit for code reuse and polimorphic tricks not for modeling.

Data and encapsulation
----------------------

* setters and getters for all fields is not encapsulation, it is fasole
* prefer immutable data
  final classes, final fields, getters, no behaviour
  Maybe acompanied by a mutable builder class.
* never have global unencapsulated data. Treat singletons with mutable data with respect.
  Every truly global mutable data is a database implementation!
* you **have to** encapsulate mutable fields.
* getters that return mutable data structures break encapsulation
  See Collections.unmodifiable etc
* dont accidentally introduce mutually recursive types. 
  Just because you need that data hereee!
  player.getGame().boardState().currentPlayer().hello()
* dont keep or create references to random far away types 
  cause you need to call that method.
  Fix your dependencies.
* strive to have a class tree not a class graph. 
  If everyting imports everyting then meh, its like a huge class split in many files.
* beware of hidden/semantic dependencies
  ex: I dont call foo.init() cause i know from the implementation of foo that foo.process() calls it
* sometimes it makes more sense to pass part of an object than the whole thing.
  heightStatistic.addSample(person.height) wont make the stats class depend on person for no reason

sane mutable data
~~~~~~~~~~~~~~~~~

* you **have to** encapsulate mutable fields.
* decide who is the single responsible actor for changing the stuff.
* better copy than sorry
* dont overexpose mutation.  Exposing a task queue as a util.Queue. 
  Do you expect clients to call clear on that, cause they will.
  Encapsulate the thing is something that only exposes push(newtask),  pop
* enforce invariants/validation: yey we have a negative length

Classes, oop and all the rest
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Modelling has-A. Use the collection interfaces from util.
Usually easy to model and works.

Modelling is-A. Hi Liskov and Ontology.
Careful with taxonomic intuition. Mutable Circle is neiter a Elipse not the other way around.
Is-A is almost always false.
Just dont. 
Liskov.
Use inheritance only for reusing and customizing code and for polimorphic triks.

Number of methods.. remember the 7 slots?

Split low cohesion ones. Most methods should access most fields.

Do create tiny value classes, even encapsulating one simple scalar, 
dont forget about equals and hash.  ::

   class Distance {
       final long mm;
       long meters();
       int km();
   }
   class Heading{
       long heading;
   }


The graph of it all
-------------------

The dependency graph of your program has classes as nodes.
May you find strongly connected components in it and good names for them.

.. image :: Graph_Condensation.svg.png
    :scale: 50%


Systems
-------

databases

* the global mutable state of an app
* data structure / entity server
* transactional concurency primitive.

caches

* fast fake databases

Task queues

* simple distributed system
* async concurrent workers take jobs from a queue and run them
  results in a result queue or in a db
* async concurrent code submits tasks to the queue

remote services

* ex: send sms
* query map data
* ask for the weather etc. 
* usually stateless.


The failings of OOP
-------------------

Up front modelling almost always fails.
True Liskov inheritance is so rare that it makes little sense to model with it.

Non tree like composition graphs are hard to reason about.
Composition of behaviour is hard. 
Composition of data works, but then it is simply db like entity data modelling.

Composition of behavioural classes can be tree like, thus implementing a layered system.
This is sane to reason about.

But as soon as we insist on not duplicating code we will get cross layer code,
reused along the tree. Making the tree into a directed graph.
Communication in a layered tree composition can also be cumbersome as it can
only happen along a common ancestor. ::

    M -- A -- B -- L
           -- C
      -- D -- L

A uses B and C etc

If B wants to talk to D then it sends a message via  B->A->M->D

It is now that we should recognize that this is no longer about has-a relations.
They are knows-a, communicates-with. In this sense the ubiquitous car has engine example becomes:
Car has engine, engine interacts-with gasoline.
Using of course the method Exhaust gasolineInsideMe.explodeYourself(temperature, pressure)

L is common, so we refer just to one, so the tree is now a dag.

Ok but at least D depends on L, so changing D will not influence L
Well yes unless now we want something different from L so now we need another L'
Can we change L without changing D? Seems so if we don't change the contract.
Bul L's  behavior is essentially D's behaviour, so any change in L will change
the semantics of D, M , even though it may not change any code in them.
Change of behaviour seems hard to contain.

If L is a Adt, ex a hash map, changing it from linked to open address is contained.
But this is cause a hash map is an abstraction so good it hides implementation.
But for ex map is not a good abstraction, hashmap is not interchangeable with treemap
it does have a different public requirement from the keys!

What really works
-----------------

Abstractions so good that they completely hide the underlying system.
Examples:

* full DSL's like sql, regex
* ADT's : hashmap arraylist (but not list, map)
* primitive streams: files
* protocols: sockets, http, odbc
* single-purpose libraries: libjpg

Garbage collection. It revolutionized programming.

Immutable data structures and pure functions.

Good IDE's

Talking to people and code review.

Knowledge of the domain the software is written for.


What works most of the time
---------------------------

Testing

Favoring complex data than complex code.

Sugar. akka {'e': [3, 4, 5]} for a map with a list value

Frameworks.

Plugin architectures, where components are separated by published protocols.
Ex: microservices, dependency injectors

Writing documentation outside of comments.
