package deduplicator;

import java.io.File;
import java.io.IOException;
import java.util.*;

class FileGroup<T> {
    private final Map<T, List<File>> groups = new HashMap<>();

    void put(T key, File file){
        if (!groups.containsKey(key)){
            groups.put(key, new ArrayList<>());
        }
        groups.get(key).add(file);
    }

    FileGroup<T> withoutOneElementGroups(){
        FileGroup<T> result = new FileGroup<>();
        for(T key: groups.keySet()){
            List<File> files = groups.get(key);
            if (files.size() > 1) {
                result.groups.put(key, files);
            }
        }
        return result;
    }

    Collection<List<File>> getGroups(){
        return groups.values();
    }

    String prettyPrint(){
        StringBuilder sb = new StringBuilder();
        for(T key : groups.keySet()){
            sb.append("group: ");
            sb.append(key);
            sb.append("\n");
            for (File file: groups.get(key)){
                sb.append("    ");
                sb.append(file);
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    /**
     * Split the group into finer ones by using a second classifier.
     * The group key will be a tuple of the former key and the one introduced by the new classifier.
     */
    <U> FileGroup<Tuple<T, U>> groupBy(Classifier<U> classifier) throws IOException {

        FileGroup<Tuple<T, U>> ret = new FileGroup<>();

        for(T key : groups.keySet()){
            for (File file : groups.get(key)) {
                U subkey = classifier.classify(file);
                Tuple<T, U> compositeKey = new Tuple<>(key, subkey);
                ret.put(compositeKey, file);
            }
        }
        return ret;
    }

    int size(){
        int ret = 0;
        for(List<File> files: groups.values()){
            ret += files.size();
        }
        return ret;
    }

    static <T> FileGroup<T> groupBy(Classifier<T> classifier, List<File> files) throws IOException {
        FileGroup<T> groups = new FileGroup<>();
        for (File file : files) {
            groups.put(classifier.classify(file), file);
        }
        return groups;
    }
}
