package deduplicator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DeDuplicator{
    private static final Logger LOG = Logger.getLogger(DeDuplicator.class.getName());

    private final List<Classifier<?>> classifierChain;

    public DeDuplicator(Precision precision){
        this.classifierChain = new ArrayList<>();
        this.classifierChain.add(File::length);
        this.classifierChain.add(Functions::fileHeaderHash);

        if (precision == Precision.FILE_MD5) {
            this.classifierChain.add(Functions::computeMd5);
        }
    }

    private FileGroup<?> applyClassifierChain(
            List<File> files,
            List<Classifier<?>> classifiers) throws IOException {

        if (classifiers.size() == 0){
            throw new IllegalArgumentException();
        }

        FileGroup<?> group = FileGroup.groupBy(classifiers.get(0), files);
        logProgress(classifiers.get(0), group);

        for (int i = 1; i < classifiers.size(); i++) {
            group = group.groupBy(classifiers.get(i))
                    .withoutOneElementGroups();
            logProgress(classifiers.get(i), group);
        }
        return group;
    }

    private void logProgress(Classifier<?> classifier, FileGroup<?> group) {
        LOG.info(String.format("classifier %s finished", classifier));
        LOG.info(String.format("   files remaining %d", group.size()));
    }

    private Stream<File> getFiles(File directory, long ignoreSmallerThan) throws IOException {
        return Files.walk(directory.toPath())
                .filter(path -> Files.isRegularFile(path))
                .map(Path::toFile)
                .filter(file -> file.length() > ignoreSmallerThan);
    }

    public List<List<File>> run(File root) throws IOException {
        List<File> files = getFiles(root, 1).collect(Collectors.toList());

        FileGroup<?> grouped = applyClassifierChain(files, classifierChain);
        return new ArrayList<>(grouped.getGroups());
    }
}
