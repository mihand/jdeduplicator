package deduplicator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

class Functions{
    private static MessageDigest getAlgoritm(String algorithm){
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("need a java with md5 hash");
        }
    }

    private static String digestToHex(byte[] digest){
        StringBuilder sb = new StringBuilder();
        for (byte b : digest){
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    static String computeMd5(byte[] fileContent) {
        MessageDigest md = getAlgoritm("MD5");
        md.update(fileContent);
        return digestToHex(md.digest());
    }

    static String computeMd5(File file) throws IOException {
        MessageDigest md = getAlgoritm("MD5");
        byte[] buffer = new byte[1024];
        int nread;

        try(InputStream stream = new FileInputStream(file)) {
            nread = stream.read(buffer);

            while (nread > 1) {
                md.update(buffer, 0, nread);
                nread = stream.read(buffer);
            }
            return digestToHex(md.digest());
        }
    }

    static byte[] readFileHeader(File file) throws IOException {
        try (FileInputStream stream = new FileInputStream(file)) {
            byte[] buffer = new byte[1024];
            byte[] header;
            int read = stream.read(buffer);
            header = Arrays.copyOf(buffer, read);
            return header;
        }
    }

    static String fileHeaderHash(File file) throws IOException {
        return computeMd5(readFileHeader(file));
    }
}
