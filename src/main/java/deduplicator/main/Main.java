package deduplicator.main;

import deduplicator.DeDuplicator;
import deduplicator.Precision;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        if (args.length != 1){
            System.err.println("usage: script directoryPath");
        }

        DeDuplicator app = new DeDuplicator(Precision.FILE_MD5);

        File root = new File(args[0]);
        List<List<File>> duplicateList = app.run(root);

        for (int i = 0; i < duplicateList.size(); i++) {
            System.out.println("duplicate set " + i);
            for(File file: duplicateList.get(i)){
                System.out.print("    ");
                System.out.println(file);
            }
        }
   }
}
