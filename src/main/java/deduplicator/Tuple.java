package deduplicator;

public final class Tuple<T, U> {
    private final T first;
    private final U second;

    public Tuple(T fist, U second){
        this.first = fist;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public U getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;
        if (! (object instanceof Tuple))
            return false;

        Tuple other = (Tuple) object;
        return other.first.equals(first) && other.second.equals(second);
    }

    @Override
    public int hashCode() {
        // treat fields as elements in a list. polynomial hashing
        // see https://docs.oracle.com/javase/8/docs/api/java/util/List.html#hashCode--
        int hash = 0;
        hash += first.hashCode() * 31;
        hash += second.hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Tuple(%s, %s)", first, second);
    }
}
