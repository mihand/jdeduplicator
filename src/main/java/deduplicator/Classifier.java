package deduplicator;

import java.io.File;
import java.io.IOException;

@FunctionalInterface
interface Classifier<T> {
    T classify(File file) throws IOException;
}
